<?php

namespace App\Exception;


class PropertyNotFoundException extends \Exception
{
    /**
     * PropertyNotFoundException constructor.
     * @param string $class
     * @param string $property
     */
    public function __construct(string $class, string $property)
    {
        parent::__construct();
        $this->message = 'In class ' . $class . ' property ' . $property . 'does not exist';
    }
}