<?php

namespace App\Entity;

use App\Exception\PropertyNotFoundException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GithubRepoRepository")
 */
class GithubRepo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $forks;

    /**
     * @ORM\Column(type="integer")
     */
    private $stargazers;

    /**
     * @ORM\Column(type="integer")
     */
    private $watchers;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_release_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_release_author;

    /**
     * @ORM\Column(type="integer")
     */
    private $pull_requests;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language_1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language_2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language_3;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $licence;

    public function __get($property) {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
        throw new PropertyNotFoundException(self::class, $property);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getForks(): ?int
    {
        return $this->forks;
    }

    public function setForks(int $forks): self
    {
        $this->forks = $forks;

        return $this;
    }

    public function getStargazers(): ?int
    {
        return $this->stargazers;
    }

    public function setStargazers(int $stargazers): self
    {
        $this->stargazers = $stargazers;

        return $this;
    }

    public function getWatchers(): ?int
    {
        return $this->watchers;
    }

    public function setWatchers(int $watchers): self
    {
        $this->watchers = $watchers;

        return $this;
    }

    public function getLastReleaseAt(): ?\DateTimeInterface
    {
        return $this->last_release_at;
    }

    public function setLastReleaseAt(\DateTimeInterface $last_release_at): self
    {
        $this->last_release_at = $last_release_at;

        return $this;
    }

    public function getLastReleaseAuthor(): ?string
    {
        return $this->last_release_author;
    }

    public function setLastReleaseAuthor(string $last_release_author): self
    {
        $this->last_release_author = $last_release_author;

        return $this;
    }

    public function getPullRequests(): ?int
    {
        return $this->pull_requests;
    }

    public function setPullRequests(int $pull_requests): self
    {
        $this->pull_requests = $pull_requests;

        return $this;
    }

    public function getLanguage1(): ?string
    {
        return $this->language_1;
    }

    public function setLanguage1(string $language_1): self
    {
        $this->language_1 = $language_1;

        return $this;
    }

    public function getLanguage2(): ?string
    {
        return $this->language_2;
    }

    public function setLanguage2(string $language_2): self
    {
        $this->language_2 = $language_2;

        return $this;
    }

    public function getLanguage3(): ?string
    {
        return $this->language_3;
    }

    public function setLanguage3(string $language_3): self
    {
        $this->language_3 = $language_3;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(string $licence): self
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Check if Entity should be refreshed
     * @return bool
     */
    public function isFresh() : bool {
        $lastFreshTime = (new \DateTime())->modify('-' . getenv("REPO_FRESH_HOURS") . ' hours');
        return $lastFreshTime < $this->updated_at;
    }
}
