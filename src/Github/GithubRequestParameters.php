<?php

namespace App\Github;

class GithubRequestParameters
{
    private $numberOfLanguages = 3;
    private $numberOfReleases = 1;

    public function __construct(int $numberOfLanguages, int $numberOfReleases)
    {
        if(!empty($numberOfLanguages)) {
            $this->numberOfLanguages = $numberOfLanguages;
        }
        if(!empty($numberOfReleases)) {
            $this->numberOfReleases = $numberOfReleases;
        }
    }

    public function toArray() {
        return [
            'number_of_languages' => $this->numberOfLanguages,
            'number_of_releases' => $this->numberOfReleases
        ];
    }
}