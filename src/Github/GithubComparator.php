<?php declare(strict_types=1);

namespace App\Github;

use App\Entity\GithubRepo;

class GithubComparator
{
    /**
     * @var string
     */
    private $param;
    /**
     * @var GithubRepo
     */
    private $repo1;

    /**
     * @var GithubRepo
     */
    private $repo2;

    public function setParam($param) : void
    {
        $this->param = $param;
    }

    public function setRepo1(GithubRepo $repo1) {
        $this->repo1 = $repo1;
    }

    public function setRepo2(GithubRepo $repo2) {
        $this->repo2 = $repo2;
    }

    /**
     * @return string
     */
    public function toJSON() : string
    {
        $paramName = $this->param;
        $array = [
            'parameter' => $this->param,
            'value_1' => $this->repo1->$paramName,
            'value_2' => $this->repo2->$paramName,
            'result' => $this->repo1->{$this->param} <=> $this->repo2->{$this->param}
        ];

        return json_encode($array);
    }

}