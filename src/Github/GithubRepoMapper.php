<?php

namespace App\Github;


use App\Entity\GithubRepo;

class GithubRepoMapper
{
    /**
     * Maps JSON object received from Github API to GithubRepo object
     * @see GithubClient::getGithubRepo()
     * @param $json
     * @return GithubRepo
     */
    public function fromAPI($json) : GithubRepo
    {
        $repo = new GithubRepo();
        $repo->setOwner($json->data->repository->owner->login);
        $repo->setName($json->data->repository->name);
        if(!empty($json->data->repository->forks)) {
            $repo->setForks($json->data->repository->forks->totalCount);
        } else {
            $repo->setForks(0);
        }
        if(!empty($json->data->repository->stargazers)) {
            $repo->setStargazers($json->data->repository->stargazers->totalCount);
        } else {
            $repo->setStargazers(0);
        }
        if(!empty($json->data->repository->watchers)) {
            $repo->setWatchers($json->data->repository->watchers->totalCount);
        } else {
            $repo->setWatchers(0);
        }
        if(!empty($json->data->repository->releases->nodes[0])) {
            $repo->setLastReleaseAt(new \DateTime($json->data->repository->releases->nodes[0]->createdAt));
        }
        if(!empty($json->data->repository->releases->nodes[0])) {
            $repo->setLastReleaseAuthor($json->data->repository->releases->nodes[0]->author->name);
        } else {
            $repo->setLastReleaseAuthor("");
        }
        if(!empty($json->data->repository->pullRequests)) {
            $repo->setPullRequests($json->data->repository->pullRequests->totalCount);
        } else {
            $repo->setPullRequests(0);
        }
        if(!empty($json->data->repository->languages->nodes[0])) {
            $repo->setLanguage1($json->data->repository->languages->nodes[0]->name);
        } else {
            $repo->setLanguage1("");
        }
        if(!empty($json->data->repository->languages->nodes[1])) {
            $repo->setLanguage2($json->data->repository->languages->nodes[1]->name);
        } else {
            $repo->setLanguage2("");
        }
        if(!empty($json->data->repository->languages->nodes[2])) {
            $repo->setLanguage3($json->data->repository->languages->nodes[2]->name);
        } else {
            $repo->setLanguage3("");
        }
        $repo->setCreatedAt(new \DateTime($json->data->repository->createdAt));
        if(!empty($json->data->repository->licenseInfo)) {
            $repo->setLicence($json->data->repository->licenseInfo->name);
        } else {
            $repo->setLicence("");
        }
        $repo->setUpdatedAt(new \DateTime());
        return $repo;
    }

    /**
     * @param GithubRepo $repo
     * @return string
     */
    public function toJson(GithubRepo $repo) : string {
        $array = [];
        $array['owner'] = $repo->getOwner();
        $array['name'] = $repo->getName();
        $array['forks'] = $repo->getForks();
        $array['stargazers'] = $repo->getStargazers();
        $array['watchers'] = $repo->getWatchers();
        $array['last_release_at'] = $repo->getLastReleaseAt();
        $array['last_release_author'] = $repo->getLastReleaseAuthor();
        $array['pull_requests'] = $repo->getPullRequests();
        $array['language_1'] = $repo->getLanguage1();
        $array['language_2'] = $repo->getLanguage2();
        $array['language_3'] = $repo->getLanguage3();
        $array['created_at'] = $repo->getCreatedAt();
        $array['licence'] = $repo->getLicence();
        $array['updated_at'] = $repo->getUpdatedAt();
        return json_encode($array);
    }
}