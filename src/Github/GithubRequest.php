<?php

namespace App\Github;


class GithubRequest
{
    private $owner;
    private $name;

    public function __construct(string $owner, string $name)
    {
        $this->owner = $owner;
        $this->name = $name;
    }

    /**
     * Returns GraphQL request that should be passed to Client
     * @see \EUAutomation\GraphQL\Client
     * @return string
     */
    public function build() : string {
        return <<<QUERY
query(\$number_of_languages:Int!, \$number_of_releases:Int!) {
  repository(owner: "$this->owner", name: "$this->name") {
    owner {
      login
    },
    name,
    forks {
      totalCount
    },
    stargazers {
      totalCount
    },
    watchers {
      totalCount
    },
    releases(last: \$number_of_releases) {
      nodes {
        author {
          name
        },
        createdAt
      }
    },
    pullRequests {
      totalCount
    },
    languages(first: \$number_of_languages) {
      nodes {
        name
      }
    },
    createdAt,
    licenseInfo {
      name
    }
  }
}
QUERY;

    }
}