<?php
namespace App\Github;

use App\Entity\GithubRepo;

class GithubClient
{
    private $client;
    private $request;
    private $parameters;
    private $token;

    /**
     * GithubClient constructor.
     * @param \EUAutomation\GraphQL\Client $client
     * @param GithubRequest $request
     * @param GithubRequestParameters $parameters
     * @param string $token
     */
    public function __construct(\EUAutomation\GraphQL\Client $client, GithubRequest $request, GithubRequestParameters $parameters, string $token)
    {
        $this->client = $client;
        $this->request = $request;
        $this->parameters = $parameters;
        $this->token = $token;
    }

    /**
     * @param GithubRepoMapper $mapper
     * @return GithubRepo
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    public function getGithubRepo(GithubRepoMapper $mapper) : GithubRepo
    {
        $headers = [
            'Authorization' => 'bearer ' . $this->token
        ];
        $json = $this->client->json($this->request->build(), $this->parameters->toArray(), $headers);
        return $mapper->fromAPI($json);
    }



}