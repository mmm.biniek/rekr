<?php declare(strict_types=1);

namespace App\Controller;

use App\Github\GithubClient;
use App\Github\GithubComparator;
use App\Github\GithubRepoMapper;
use App\Github\GithubRequest;
use App\Github\GithubRequestParameters;
use App\Entity\GithubRepo;
use Doctrine\Common\Persistence\ObjectRepository;
use EUAutomation\GraphQL\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GithubController extends AbstractController
{

    private const NUMBER_OF_LANGUAGES = 3;
    private const NUMBER_OF_RELEASES = 1;

    /**
     * @var GithubRepoMapper
     */
    private $mapper;

    public function __construct()
    {
        $this->mapper = new GithubRepoMapper();
    }

    /**
     * @Route("/github/repo/{owner}/{name}")
     *
     * Searches in DB if repo is cached. If not - sends new request to GitHub API.
     *
     * @param string $owner
     * @param string $name
     * @return Response
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    public function getRepo(string $owner, string $name) : Response
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(GithubRepo::class);
        $repo = $this->getFreshRepo($repository, $owner, $name);
        return Response::create($this->mapper->toJson($repo));
    }

    /**
     * @Route("/github/repo/{owner}/{name}", methods={"PUT"})
     *
     * Updates repo in DB.
     *
     * @param string $owner
     * @param string $name
     * @return Response
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    public function updateRepo(string $owner, string $name) : Response
    {
        $repo = $this->getFreshRepo($owner, $name);
        return Response::create($this->mapper->toJson($repo));
    }

    /**
     * @Route("/github/compare/{param}")
     * Compare two Github repos.
     * @param string $param
     * @param Request $request
     * @return Response
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    public function compareRepos(string $param, Request $request) : Response
    {
        $owner1 = $request->query->get('owner1');
        $name1 = $request->query->get('name1');
        $owner2 = $request->query->get('owner2');
        $name2 = $request->query->get('name2');

        $repository = $this->getDoctrine()->getManager()->getRepository(GithubRepo::class);
        $repo1 = $this->getFreshRepo($repository, $owner1, $name1);
        $repo2 = $this->getFreshRepo($repository, $owner2, $name2);

//        $comparator = new GithubComparator($param, $repo1, $repo2);
        $comparator = new GithubComparator();
        $comparator->setParam($param);
        $comparator->setRepo1($repo1);
        $comparator->setRepo2($repo2);
        return Response::create($comparator->toJSON());
    }

    /**
     * Check for repo in DB first and if not found send request to Github API.
     *
     * @param ObjectRepository $repository
     * @param string $owner
     * @param string $name
     * @return GithubRepo
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    private function getFreshRepo(ObjectRepository $repository, string $owner, string $name) : GithubRepo
    {
        $repo = $repository->findOneBy(['owner' => $owner, 'name' => $name]);
        if(empty($repo) || !$repo->isFresh()) {
            $repo = $this->getRepoFromAPI($owner, $name);
        }
        return $repo;
    }

    /**
     * Sends request to Github API for fresh GithubRepo.
     *
     * @param $owner
     * @param $name
     * @return GithubRepo
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLInvalidResponse
     * @throws \EUAutomation\GraphQL\Exceptions\GraphQLMissingData
     */
    private function getRepoFromAPI($owner, $name) : GithubRepo
    {
        $graphQLUrl = getenv('GITHUB_API_URL');
        $graphQLClient = new Client($graphQLUrl);
        $parameters = new GithubRequestParameters(self::NUMBER_OF_LANGUAGES, self::NUMBER_OF_RELEASES);
        $request = new GithubRequest($owner, $name);
        $token = getenv('GITHUB_TOKEN');
        $githubClient = new GithubClient($graphQLClient, $request, $parameters, $token);
        $repo = $githubClient->getGithubRepo($this->mapper);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($repo);
        $manager->flush();
        return $repo;
    }


}