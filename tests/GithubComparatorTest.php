<?php declare(strict_types=1);

namespace App\Tests;
use PHPUnit\Framework\TestCase;

use App\Entity\GithubRepo;
use App\Github\GithubComparator;

class GithubComparatorTest extends TestCase
{
    public function testToJSONCompareIntegers() : void
    {
        $comparator = new GithubComparator();
        $comparator->setParam('stargazers');
        $repo1 = new GithubRepo();
        $repo1->setStargazers(10);
        $comparator->setRepo1($repo1);
        $repo2 = new GithubRepo();
        $repo2->setStargazers(12);
        $comparator->setRepo2($repo2);
        $json = $comparator->toJSON();
        $expectedJson = json_encode(
            [
                'parameter' => 'stargazers',
                'value_1' => 10,
                'value_2' => 12,
                'result' => -1
            ]
        );
        $this->assertEquals($expectedJson, $json);
    }

    public function testToJSONCompareDates() : void
    {
        $today = new \DateTime();
        $yesterday = (new \DateTime())->add(\DateInterval::createFromDateString('yesterday'));
        $comparator = new GithubComparator();
        $comparator->setParam('created_at');
        $repo1 = new GithubRepo();
        $repo1->setCreatedAt($today);
        $comparator->setRepo1($repo1);
        $repo2 = new GithubRepo();
        $repo2->setCreatedAt($yesterday);
        $comparator->setRepo2($repo2);
        $json = $comparator->toJSON();
        $expectedJson = json_encode(
            [
                'parameter' => 'created_at',
                'value_1' => $today,
                'value_2' => $yesterday,
                'result' => 1
            ]
        );
        $this->assertEquals($expectedJson, $json);
    }
}