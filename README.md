# GitHub repository stats comparator

## Purpose
The purpose of this project is to compare two GitHub repositories by comparing basic statistics between them.  
This application stores repositories' statistics in database and updates them on demand or when they expire 
(time of expiration can be set in .env). It is supposed to lower workload on GitHub API and make comparison 
quicker.  
Application uses GitHub v4 API (graphQL) as external source. 

## Installation
Use cd to directory rekr/docker and use:   
`docker-compose build --no-cache`  
`docker-compose up -d`  
It should create four containers:  
*  docker_mysql_1
*  docker_php_1
*  docker_phpmyadmin_1 (not necessary for project, used for debugging)
*  docker_nginx_1

You can check this typing in termianal:  
`docker container ls`

Run migrations when in rekr/bin:  
`php console doctrine:migrations:migrate`

It is important to put GitHub API token inside .env file (`GITHUB_TOKEN`). You can generate token on GitHub page: [https://github.com/settings/tokens](https://github.com/settings/tokens)

## Usage
This application runs on [127.0.0.1:8080](http://127.0.0.1:8080) by default.  
You can check all stats that are gathered by application by sending GET request with owner and name of repository, eg.:  
[http://127.0.0.1:8080/github/repo/symfony/symfony](http://127.0.0.1:8080/github/repo/symfony/symfony)  
will show what stats that are gathered and stored in database for symfony.

Those statistics are cached in database, so they can be out of date. You can force GitHub API call by sending
PUT request to the same address. It will refresh entity in database.

To compare a parameter between two repositories you can use eg.:
[http://127.0.0.1:8080/github/compare/stargazers?owner1=symfony&name1=symfony&owner2=laravel&name2=laravel](http://127.0.0.1:8080/github/compare/stargazers?owner1=symfony&name1=symfony&owner2=laravel&name2=laravel)  
which returns JSON containing information about those two parameters and comparison. Standard return for PHP spaceship
operator (`<=>`) is used, ie: -1 for smaller, 0 for equal and 1 when second parameter is greater in second repository.